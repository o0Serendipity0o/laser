from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable

from beam import Beam

sns.set(style="dark", palette='deep')
sns.set_context("paper")
map = sns.diverging_palette(220, 20, as_cmap=True)


def example_1():
    wlt = 1064E-9  # Longueur d'onde
    w0 = 100E-6  # w0 = rayon du faisceau au waist
    # P0 = 5E-3  # puissance : 5mW
    E0 = 1.
    zz = 0  # Distance z en m ( 0 -> plot au waist)
    n = 6  # Mode pour le faisceau hermite gaussian, horizontal
    m = 3  # Mode pour le faisceau hermite gaussian. vertical
    # meshgrid pour obtenir un plot 2D
    xaxis = np.arange(-0.001, 0.001, 0.00001)
    yaxis = np.arange(-0.001, 0.001, 0.00001)
    xx, yy = np.meshgrid(xaxis, yaxis)

    # Calcul les différents "paramètres" pour le faisceau
    k, zr, w = Beam.properties(wlt, w0, zz)

    Efield = Beam.hermite_gaussian(k, zr, w0, w, E0, zz, m, n, xx, yy)
    plt.suptitle(r'Hermite-Gaussian Mode: {},{}'.format(m, n), fontsize=15)
    # plot 2D
    plt.imshow(Efield, cmap=map, aspect="auto", extent=[-0.001 * 1E6, 0.001 * 1E6,
                                              -0.001 * 1E6, 0.001 * 1E6])
    plt.colorbar()
    plt.show()


def example_2():
    wlt = 1064E-9
    w0 = 100E-6
    P0 = 5E-3  # puissance : 5mW
    E0 = 1.

    zaxis = np.arange(-0.1, 0.1, 0.001)
    raxis = np.arange(-0.001, 0.001, 0.00001)
    zz, rr = np.meshgrid(zaxis, raxis)

    # Calcul les différents "paramètres" pour le faisceau
    k, zr, w = Beam.properties(wlt, w0, zz)

    Efield = Beam.gaussian(k, zr, w0, w, E0, zz, rr)
    Intensity = Beam.intensity(P0, w, rr)
    # Plot
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True,
                                   sharey=True)
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False,
                    right=False)

    fig.suptitle('Propagation of Gaussian Beams', fontsize=15)
    plt.ylabel('Beam radius (um)')
    plt.xlabel('z position (mm)')

    ax1.set_title("Electric field distribution")
    im1 = ax1.imshow(Efield, cmap=map, aspect="auto", extent=[-0.1 * 1000, 0.1 * 1000,
                                                    -0.001 * 1E6, 0.001 * 1E6])
    divider1 = make_axes_locatable(ax1)
    cax1 = divider1.append_axes("right", size="2%", pad=0.05)
    plt.colorbar(im1, cax=cax1)

    ax2.set_title("Transverse profile of the optical intensity")
    im2 = ax2.imshow(Intensity, cmap=map, aspect="auto", extent=[-0.1 * 1000, 0.1 * 1000,
                                                       -0.001 * 1E6, 0.001 * 1E6])

    divider2 = make_axes_locatable(ax2)
    cax2 = divider2.append_axes("right", size="2%", pad=0.05)
    plt.colorbar(im2, cax=cax2)

    plt.tight_layout()
    # Make space for title
    plt.subplots_adjust(left=0.15, bottom=0.10, top=0.85, wspace=0.8)
    plt.show()


if __name__ == '__main__':
    example_1()
    # example_2()

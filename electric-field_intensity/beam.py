import numpy as np
from scipy.special import eval_hermite
from scipy.special import eval_genlaguerre


class Beam():
    def properties(wlt, w0, zz):
        k = (2*np.pi)/wlt
        zr = (np.pi*w0**2)/wlt
        w = w0*np.sqrt(1+((zz/zr)**2))
        return k, zr, w

    def gaussian(k, zr, w0, w, E0, zz, rr):
        R = zz+((zr)**2/zz)
        phase_gouy = np.arctan(zz/zr)
        phi = k*zz-((k*rr**2)/(2.0*R))+phase_gouy
        Efield = E0*(w0/w)*np.exp(-(rr**2/w**2))*np.cos(phi)
        return Efield

    def hermite_gaussian(k, zr, w0, w, E0, zz, m, n, xx, yy):
        phase = (1+n+m)*np.arctan(zz/zr)
        phi = k*zz-((k*(xx**2+yy**2)*zz)/(2.0*(zz**2+zr**2)))+phase
        # Hermite_gaussian
        mm = np.sqrt(2)*xx/w
        nn = np.sqrt(2)*yy/w
        Hm = eval_hermite(m, mm)
        Hn = eval_hermite(n, nn)
        # electric field
        Efield = E0*(w0/w)*np.exp(-((xx**2+yy**2)/w**2))*np.cos(phi)*Hm*Hn
        return Efield

    def laguerre_gaussian(k, zr, w0, w, E0, zz, p, l, RR, TT):
        phase = np.arctan(zz/zr)*(2*p+l+1)
        phi = k*zz-((k*RR**2*zz)/(2.0*(zz**2+zr**2)))+phase+(l*TT)
        # Laguerre_Gaussian
        mm = 2*((RR**2)/(w**2))
        L = (np.sqrt(2)*RR/w)**l*eval_genlaguerre(p, l, mm)
        Efield = E0*(w0/w)*np.exp(-(RR**2/w**2))*np.cos(phi)*L

        return Efield

    def intensity(P0, w, rr):
        Intensity = (P0/((np.pi*w**2)/2))*np.exp(-2*((rr**2)/(w**2)))

        return Intensity

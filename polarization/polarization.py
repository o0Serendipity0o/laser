import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
# import mplcyberpunk


sns.set(style="whitegrid", palette='deep')
sns.set_context("paper")
# plt.style.use("cyberpunk")


def cw(t, w, ax, ay, phi1, phi2):
    x = ax * np.cos(w * t + phi1)
    y = ay * np.sin(w * t + phi2)
    z = t
    return x, y, z


def plot_2D(ax, x, y):
    ax.plot(x, y)
    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')
    ax.set_xlim(-1.25, 1.25)
    ax.set_ylim(-1.25, 1.25)


def plot_3D(ax, tmax, x, y, z, x1, y1, z1, x2, y2, z2):
    val = [1, 0, 0]
    val1 = [tmax + 5, 0, 0]
    for v in range(3):
        a = [val1[v - 0], 0]
        b = [val[v - 1], -val[v - 1]]
        c = [val[v - 2], -val[v - 2]]
        ax.plot(a, b, c, 'gray', linewidth=1)
    ax.plot(z, y, x)
    ax.plot(z1, y1, x1)
    ax.plot(z2, y2, x2)
    ax.set_axis_off()
    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')


def main():
    phi1 = 0
    phi2 = 0
    ax = 1
    ay = 0
    w = 1
    res = 500
    tmin = 0
    tmax = 7 * np.pi
    dt = (tmax - tmin) / res
    t = np.arange(tmin + 2 * dt / 2, tmax - 2 * dt / 2, dt)

    x, y, z = cw(t, w, ax, ay, phi1, phi2)
    x1, y1, z1 = cw(t, w, 0, 1, phi1, 0)
    x2, y2, z2 = cw(t, w, 1, 1, phi1, 0)
    x2, y2, z2 = cw(t, w, 1, 1, phi1, 0)
    x11, y11, z11 = cw(t, w, 0.4, 1, phi1, 0)
    x10, y10, z10 = cw(t, w, 0.4, 1, phi1, 0)

    plt.close('all')
    fig = plt.figure(figsize=(4, 9), linewidth=10, edgecolor="#04253a")
    ax1 = fig.add_subplot(211, projection='3d')
    plot_3D(ax1, tmax, x, y, z, x1, y1, z1, x2, y2, z2)
    ax2 = plt.subplot(212)
    plot_2D(ax2, x2, y2)
    plt.show()


if __name__ == '__main__':
    main()

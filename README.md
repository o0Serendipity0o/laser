# laser

In the Electric field and Intensity folder a code to plot:   
the propagation oh the electric field and intensity with shape.
The Laguerre Gaussian and Hermite Gaussian mode.

![alt text](images/Efield_intensity.png "Efield gaussian beam")  

![alt text](images/HG_beam.png "Hermite gaussian mode")  


In the polazization folder, a 3D and 2D plot of the different type of polarization  

![alt text](images/circular_polarization.png "Circular polarization ")  

In the short puls folder, a plot of pulses with different parameters (CEP, and linear chirp)  

![alt text](images/short_pulse.png "Short pulses")

## Usage
To run the files:  
```bash
python main_beam.py

python polarization.py

python short_pulse.py
```

 @o0Serendipity0o

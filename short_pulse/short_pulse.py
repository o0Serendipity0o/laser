from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns
import mplcyberpunk


sns.set(style="whitegrid", palette='deep')
sns.set_context("paper")
plt.style.use("cyberpunk")


def chirp_pulse_gaussien(Dt, w0, t, phi, phi2):
    sig = Dt / (np.sqrt(2 * np.log(2)))
    E_t = np.exp(-((t) / sig)**2) * np.cos(w0 * t - (phi + phi2 * t**2))
    return E_t


def enveloppe(Dt, t):
    sig = Dt / (np.sqrt(2 * np.log(2)))
    gaussian = np.exp(-((t) / sig)**2)
    return gaussian


def main():
    lda = 800e-9
    c = 3e8
    w0 = (2 * np.pi * c) / lda
    # w0 = 2.35e15
    Dt = 5e-15
    phi = 0
    phi2 = 0.15e30
    tmin = -15e-15
    tmax = 15e-15
    res = 250
    dt = (tmax - tmin) / res
    t = np.arange(tmin + dt / 2, tmax - dt / 2, dt)

    E_t = chirp_pulse_gaussien(Dt, w0, t, phi, 0)
    E1_t = chirp_pulse_gaussien(Dt, w0, t, np.pi/2, 0)
    E2_t = chirp_pulse_gaussien(Dt, w0, t, 0, -phi2)
    E3_t = chirp_pulse_gaussien(Dt, w0, t, 0, phi2)
    env = enveloppe(Dt, t)

    fig = plt.figure(figsize=(9, 6), linewidth=10, edgecolor="#04253a", tight_layout=True)
    fig.suptitle('Short Pulse', fontsize=14)
    ax1 = plt.subplot(221)
    ax1.plot(t, E_t)
    ax1.plot(t, env, 'orange')
    ax1.plot(t, -env, 'orange')
    ax1.set_title('CEP =0', fontsize=12)
    ax1.set_xlabel("t(fs)", fontsize=12)
    ax1.set_ylabel("E(t)", fontsize=12)
    ax2 = plt.subplot(222)
    ax2.plot(t, E1_t)
    ax2.plot(t, env, 'orange')
    ax2.plot(t, -env, 'orange')
    ax2.set_title('CEP =$\pi/2$', fontsize=12)
    ax2.set_xlabel("t(fs)", fontsize=12)
    ax2.set_ylabel("E(t)", fontsize=12)
    ax3 = plt.subplot(223)
    ax3.plot(t, E2_t)
    ax3.plot(t, env, 'orange')
    ax3.plot(t, -env, 'orange')
    ax3.set_title(r"-$\varphi''$", fontsize=12)
    ax3.set_xlabel("t(fs)", fontsize=12)
    ax3.set_ylabel("E(t)", fontsize=12)
    ax4 = plt.subplot(224)
    ax4.plot(t, E3_t)
    ax4.plot(t, env, 'orange')
    ax4.plot(t, -env, 'orange')
    ax4.set_xlabel("t(fs)", fontsize=12)
    ax4.set_ylabel("E(t)", fontsize=12)
    ax4.set_title(r"+$\varphi''$", fontsize=12)
    ax4.annotate(r"$\varphi'' = a*t²$", fontsize=14, xy=(410, 15), xycoords='figure pixels')
    plt.show()


if __name__ == '__main__':
    main()
